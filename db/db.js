var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./mydb.sqlite3', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the in-memory SQlite database.');
  createTable();
});

const createTable = () => {
	console.log("create database table contacts");
	db.run("CREATE TABLE IF NOT EXISTS categories (cat_id INTEGER PRIMARY KEY, cat_desc TEXT, cat_name TEXT)",function(err,table){
		if(err){
			console.log("Categories Table Already Exists",err);
		}else{
			console.log("Categories Table Created",table);
		}
	});

	db.run("CREATE TABLE IF NOT EXISTS products (pro_id INTEGER PRIMARY KEY, pro_name TEXT, cat_id INTEGER NOT NULL, pro_desc TEXT)",function(err,table){
		if(err){
			console.log("Products Table Already Exists",err);
		}else{
			console.log("products Table Created",table);
		}
	});
}

module.exports = {
	all : function(query, cb) {
		db.all(query, function(err, result) {
			if (err) {
		    	cb(err,null);
		    }else{
		    	cb(null,result);
			}
		});
	},
	run : function(query, data, cb) {
		db.run(query, data, function(err, result) {
			if (err) {
		    	cb(err,null);
		    }else{
		    	cb(null,result);
			}
		});
	},
	get : function(query, data, cb) {
		db.get(query, data, function(err, result) {
			if (err) {
		    	cb(err,null);
		    }else{
		    	cb(null,result);
			}
		});
	}
}