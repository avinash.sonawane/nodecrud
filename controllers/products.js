var db = require('../db/db.js');
module.exports = {
	getCat : function (req, res) {
		let query = "select * from categories";
		db.all(query, function(err, result){
			if(err){
				res.json(err);
			}else{
				//(result && result.length > 0) ? res.json(result) : res.json([]);
				res.json(result);
			}
		});
	},
	getProducts : function (req, res) {
		let page = (req.query.page) ? req.query.page : 0;
		let limit = req.query.limit ? req.query.limit : 10;
		let query = "";
		if(req.query.limit){
			query = "select p.pro_id,p.pro_name,p.pro_desc,p.cat_id,c.cat_name from products p LEFT JOIN categories c ON p.cat_id=c.cat_id LIMIT "+limit+" OFFSET "+((page-1)*limit);
		}else{
			query = "select p.pro_id,p.pro_name,p.pro_desc,p.cat_id,c.cat_name from products p LEFT JOIN categories c ON p.cat_id=c.cat_id";
		}
		db.all(query, function(err, result){
			if(err){
				res.json(err);
			}else{
				(result && result.length > 0) ? res.json(result) : res.json([]);
			}
		});
	},
	addCategory : function(req, res){
		let cat_id = Math.floor(1000 + Math.random() * 9000);
		let cat_desc = req.body.cat_desc;
		let cat_name = req.body.cat_name;
		let query = "INSERT INTO categories (cat_id, cat_desc, cat_name) VALUES (?,?,?)";
		var data = [cat_id,cat_desc,cat_name];
		db.run(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.redirect('/index');
			}
		});
	},
	addProduct : function (req, res) {
		let pro_id = Math.floor(1000 + Math.random() * 9000);
		let pro_name = req.body.pro_name;
		let cat_id = req.body.cat_id ? req.body.cat_id : "1";
		let pro_desc = req.body.pro_desc;
		let query = "INSERT INTO products (pro_id, pro_name, cat_id, pro_desc) VALUES (?,?,?,?)";
		var data = [pro_id,pro_name,cat_id,pro_desc];
		db.run(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.redirect('/products');
			}
		});
	},
	getSingleCat : function(req, res){
		let cat_id = req.query.cat_id;
		let query = "select * from categories where cat_id = ?";
		var data = [cat_id];
		db.get(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.json(result);
			}
		});
	},
	getSingleProduct : function(req, res){
		let pro_id = req.query.pro_id;
		let query = "select * from products where pro_id = ?";
		var data = [pro_id];
		db.get(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.json(result);
			}
		});
	},
	editCategory : function(req, res){
		let cat_id = req.body.cat_id;
		let cat_desc = req.body.cat_desc;
		let cat_name = req.body.cat_name;
		let query = "UPDATE categories set cat_desc=?,cat_name=? where cat_id=?";
		let data = [cat_desc,cat_name,cat_id];
		db.run(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.redirect('/index');
			}
		});
	},
	editProduct : function(req, res){
		let pro_id = req.body.pro_id;
		let pro_name = req.body.pro_name;
		let cat_id = req.body.cat_id;
		let pro_desc = req.body.pro_desc;
		let query = "UPDATE products set pro_name=?,cat_id=?,pro_desc=? where pro_id=?";
		let data = [pro_name,cat_id,pro_desc,pro_id];
		db.run(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.redirect('/products');
			}
		});
	},
	deleteCat : function(req, res){
		let cat_id = req.query.cat_id;
		let query = "DELETE FROM categories where cat_id = ?";
		var data = [cat_id];
		db.run(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.redirect('/index');
			}
		});
	},
	deleteProduct : function(req, res){
		let pro_id = req.query.pro_id;
		let query = "DELETE FROM products where pro_id = ?";
		var data = [pro_id];
		db.run(query, data, function(err, result){
			if(err){
				res.json(err);
			}else{
				res.redirect('/products');
			}
		});
	}
}