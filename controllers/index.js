var data = require('../data.json');

module.exports = {
	getCat : function(req, res){
		res.json(data.category);
	},
	getProducts : function(req, res){
		var result = data.products;
		if(data.category && data.category.length){
			for(var i=0; i<data.category.length;i++){
				for(var j=0;j<data.products.length;j++){
					if(data.category[i].cat_id == data.products[j].cat_id){
						result[j].cat_name = data.category[i].cat_name;
					}
				}
				if(i == data.category.length -1){
					res.json(result);
				}
			}
		}else{
			res.json(result);
		}
	},
	addProduct : function(req, res){
		var length = data.products.length;
		var obj = {};
		obj.pro_id = length + 1;
		obj.pro_name = req.body.pro_name;
		obj.cat_id = req.body.cat_id ? req.body.cat_id : "1";
		obj.pro_desc = req.body.pro_desc;
		data.products.push(obj);
		res.redirect('/products');
	},
	addCategory : function (req, res) {
		var length = data.category.length;
		var obj = {};
		obj.cat_id = length + 1;
		obj.cat_desc = req.body.cat_desc;
		obj.cat_name = req.body.cat_name;
		data.category.push(obj);
		res.redirect('/index');
	},
	getSingleCat : function(req, res){
		var result = (data.category && data.category.length > 1) ? data.category.filter(x => x.cat_id == req.query.cat_id) : data.category;
		res.json(result[0]);
	},
	getSingleProduct : function(req, res){
		var result;
		result = (data.products && data.products.length > 1) ? data.products.filter(x => x.pro_id == req.query.pro_id) : data.products;
		res.json(result[0]);
	},
	editCategory : function(req, res){
		data.category.filter(function(e){
			if(e.cat_id == req.body.cat_id){
				e.cat_name = req.body.cat_name;
				e.cat_desc = req.body.cat_desc;
			}
		});
		res.redirect('/index');
	},
	editProduct : function(req, res){
		data.products.filter(function(e){
			if(e.pro_id == req.body.pro_id){
				e.pro_name = req.body.pro_name;
				e.cat_id = req.body.cat_id;
				e.pro_desc = req.body.pro_desc;
			}
		});
		res.redirect('/products');
	},
	deleteCat : function(req, res){
		var index = (data.category && data.category.length > 1) ? data.category.findIndex(e => e.cat_id == req.query.cat_id) : data.category.splice(0, 1);
		if(index !== -1){
			data.category.splice(index, 1);
		}
		res.redirect('/index');
	},
	deleteProduct : function(req, res){
		var index = (data.products && data.products.length > 1) ? data.products.findIndex(e => e.pro_id == req.query.pro_id) : data.products.splice(0, 1);
		if(index !== -1){
			data.products.splice(index, 1);
		}
		res.redirect('/products');
	}
}