var express = require('express');
var router = express.Router();
var controller = require('../controllers/index.js');

/* GET home page. */
router.get('/', function(req, res) {
  if(global.user == true){
    res.render('index');
  }else{
    res.render('login');
  }
});

router.post('/index', function(req, res) {
	if(req.body.username == "nimap" && req.body.password == "nimap"){
		global.user = true;
		res.render('index');
	}else{
		res.redirect('/');
	}
});

router.get('/index', function(req, res) {
	if(global.user == true){
		res.render('index');
	}else{
		res.redirect('/');
	}
});

router.get('/products', function(req, res) {
  if(global.user == true){
    res.render('products');
  }else{
    res.redirect('/');
  }
});

router.get('/categories', function(req, res) {
  controller.getCat(req, res);
});

router.get('/prodetails', function(req, res) {
  controller.getProducts(req, res);
});

router.post('/addProduct', function(req, res) {
  controller.addProduct(req, res);
});

router.post('/addCat', function(req, res) {
  controller.addCategory(req, res);
});

router.get('/getCat', function(req, res) {
  controller.getSingleCat(req, res);
});

router.get('/getProd', function(req, res) {
  controller.getSingleProduct(req, res);
});

router.post('/editCat', function(req, res) {
  controller.editCategory(req, res);
});

router.post('/editProd', function(req, res) {
  controller.editProduct(req, res);
});

router.get('/delCat', function(req, res) {
  controller.deleteCat(req, res);
});

router.get('/delProd', function(req, res) {
  controller.deleteProduct(req, res);
});

module.exports = router;
