var express = require('express');
var router = express.Router();
var pController = require('../controllers/products.js');

router.get('/categories', function(req, res) {
  pController.getCat(req, res);
});

router.get('/prodetails', function(req, res) {
  pController.getProducts(req, res);
});

router.post('/addProduct', function(req, res) {
  pController.addProduct(req, res);
});

router.post('/addCat', function(req, res) {
  pController.addCategory(req, res);
});

router.get('/getCat', function(req, res) {
  pController.getSingleCat(req, res);
});

router.get('/getProd', function(req, res) {
  pController.getSingleProduct(req, res);
});

router.post('/editCat', function(req, res) {
  pController.editCategory(req, res);
});

router.post('/editProd', function(req, res) {
  pController.editProduct(req, res);
});

router.get('/delCat', function(req, res) {
  pController.deleteCat(req, res);
});

router.get('/delProd', function(req, res) {
  pController.deleteProduct(req, res);
});

module.exports = router;
