$(function() {
  $.ajax({
    type: 'GET',
    url: '/p/categories',
    success: function(data) {
      $(data).each(function(i, result){
        $('#tableData').append($('<tr id="'+result.cat_id+'">')
          .append($('<td><span>').append(result.cat_id))
          .append($('<td>').append(result.cat_desc))
          .append($('<td>').append(result.cat_name))
          .append($('<td><a href="#editCatModal" class="edit" data-toggle="modal"><i class="material-icons" onClick="getCatID('+result.cat_id+')" data-toggle="tooltip" title="Edit">&#xE254;</i></a><a href="#deleteCatModal" class="delete" data-toggle="modal"><i class="material-icons" onClick="delteCatID('+result.cat_id+')" data-toggle="tooltip" title="Delete">&#xE872;</i></a>')));
      });
    }
  });
  $.ajax({
    type: 'GET',
    url: '/p/categories',
    success: function(data) {
      $(data).each(function(i, result){
        $('#cat_list').append($('<option value="'+result.cat_id+'">').append(result.cat_name));
      });
    }
  });
});
function getCatID(cat_id) {
  $.ajax({
    type: 'GET',
    url: '/p/getCat?cat_id='+cat_id,
    success: function(data) {
      $("#cat_id").val(data.cat_id);
      $("#cat_name").val(data.cat_name);
      $("#cat_desc").val(data.cat_desc);
    }
  });
}
function delteCatID(cat_id) {
  $.ajax({
    type: 'GET',
    url: '/p/delCat?cat_id='+cat_id,
    success: function(data) {
      console.log("Category Deleted");
    }
  });
}