function getData(page,limit){
  $(function() {
    $("#tableData").empty();
    $("#cat_list").empty();
    $("#cat_list2").empty();
    $.ajax({
      type: 'GET',
      url: '/p/prodetails?page='+page+'&limit='+limit,
      success: function(data) {
        prodData = data;
        $(data).each(function(i, result){
          $('#tableData').append($('<tr id="'+result.pro_id+'">')
            .append($('<td><span>').append(result.pro_id))
            .append($('<td>').append(result.pro_name))
            .append($('<td>').append(result.cat_id))
            .append($('<td>').append(result.cat_name))
            .append($('<td>').append(result.pro_desc))
            .append($('<td><a href="#editProdModal" class="edit" data-toggle="modal"><i class="material-icons" onClick="getProdID('+result.pro_id+')" data-toggle="tooltip" title="Edit">&#xE254;</i></a><a href="#deleteProdModal" class="delete" data-toggle="modal"><i class="material-icons" onClick="deleteProdID('+result.pro_id+')" data-toggle="tooltip" title="Delete">&#xE872;</i></a>')));
        });
      }
    });
    $.ajax({
      type: 'GET',
      url: '/p/categories',
      success: function(data) {
        $(data).each(function(i, result){
          $('#cat_list').append($('<option value="'+result.cat_id+'">').append(result.cat_name));
          $('#cat_list2').append($('<option value="'+result.cat_id+'">').append(result.cat_name));
        });
      }
    });
  });
}
function getProdID(pro_id) {
  $.ajax({
    type: 'GET',
    url: '/p/getProd?pro_id='+pro_id,
    success: function(data) {
      //getCategories2();
      $("#pro_id").val(data.pro_id);
      $("#prod_name").val(data.pro_name);
      $("#prod_desc").val(data.pro_desc);
      $("#cat_list2").val(data.cat_id);
    }
  });
}
function deleteProdID(pro_id) {
  $.ajax({
    type: 'GET',
    url: '/p/delProd?pro_id='+pro_id,
    success: function(data) {
      console.log("Product Deleted");
    }
  });
}

var current_page = 1;
var records_per_page = 10;
getData(current_page,records_per_page);
var prodData;

function prevPage()
{
  if(current_page < numPages()){
    document.getElementById("btn_prev").style.visibility = "hidden";
    document.getElementById("btn_next").style.visibility = "visible";
  }else{
    current_page--;
    changePage(current_page);
    getData(current_page,records_per_page);
    document.getElementById("btn_next").style.visibility = "visible";
  }
}

function nextPage()
{ 
  if(current_page > numPages()){
    document.getElementById("btn_prev").style.visibility = "visible";
    document.getElementById("btn_next").style.visibility = "hidden";
  }else{
    current_page++;
    changePage(current_page);
    getData(current_page,records_per_page);
  }
}
    
function changePage(page)
{
    var btn_next = document.getElementById("btn_next");
    var btn_prev = document.getElementById("btn_prev");
    var page_span = document.getElementById("page");
 
    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    page_span.innerHTML = current_page;
    if (page == 1) {
        btn_prev.style.visibility = "hidden";
    } else {
        btn_prev.style.visibility = "visible";
    }
}

function numPages()
{   
    return Math.ceil(prodData.length / records_per_page);
}

window.onload = function() {
    changePage(1);
};